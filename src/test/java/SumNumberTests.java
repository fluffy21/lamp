import org.example.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.example.Main.getNumsForSum;

public class SumNumberTests {

    @DisplayName("valid Array contains numbers for Sum value")
    @Test
    public void validArrayWithSuitableSum(){
        var actualResult = getNumsForSum(new int[]{-3,-1,0,1,2,5,8,9}, 6);
        Assertions.assertEquals(new Main.Result(-3,9), actualResult, "Sum numbers are not equal");
    }
}
