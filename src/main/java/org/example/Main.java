package org.example;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        int[] arr = {-3,-1,0,1,2,5,8,9};//sorted
        System.out.println(Arrays.toString(arr));
        var sum = 6;
        var result = getNumsForSum(arr, sum);
        System.out.println("\nNumbers that ends up to " + sum + ": \n" + result.firstNum+ ", " + result.secondNum);
    }
    public record Result(int firstNum, int secondNum){}
    public static Result getNumsForSum(int[] arr, int sum){
        var min = 0;
        var max = arr.length - 1;

        while(min < max) {
            if (arr[min] + arr[max] == sum)
                return new Result(arr[min], arr[max]);
            else if (arr[min] + arr[max] < sum) min++;
            else max--;
        }
        return new Result(0,0);
    }
}